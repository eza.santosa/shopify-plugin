import React, {useCallback, useState, useEffect } from 'react';
import {Card, ChoiceList, Stack, Button, Form, FormLayout} from '@shopify/polaris';
import axios from 'axios';
import Cookies from 'js-cookie';

export default function Feature() {
  const [selected, setSelected] = useState([]);
  console.log(selected);

  const handleChange = useCallback((value) => setSelected(value), []);

  useEffect(() => {    
    axios.post(`http://localhost:3000/get_features`,{
      shop: Cookies.get("shopOrigin"),
    })
    .then(res => {
        console.log(res);
        if(res.data.features){
          setSelected(res.data.features)
        }
        //console.log(res.data.courriers)
    })
  }, []);

  const handleSubmit = useCallback((_event) => {
    //console.log(selected)
    axios.post('http://localhost:3000/manage_feature', {
      shop: Cookies.get("shopOrigin"),
      courier: selected
    })
    .then(function (response) {
      console.log(response);
    })
    .catch(function (error) {
      console.log(error);
    });
  }, [selected]);

  return (
    <Card>
    <Form onSubmit={handleSubmit}>
      <FormLayout>
        <ChoiceList
        allowMultiple
        title="MANAGE ACTIVATE FEATURE"
        choices={[
            {
                label: 'Pricing',
                value: 'pricing',
            },
            {
                label: 'Waybill',
                value: 'waybill',
            },
            {
                label: 'Order Create',
                value: 'order_create',
            },
            {
                label: 'Order Pickup',
                value: 'order_pickup',
                helpText:
                '',
            },
            {
                label: 'Order Cancel',
                value: 'order_cancel',
                helpText:
                '',
            },
        ]}
        selected={selected}
        onChange={handleChange}
        />
        <Stack distribution="trailing">
          <Button primary submit>
            Save
          </Button>
        </Stack>
        </FormLayout>
      </Form>
    </Card>
    );
}
