import React, {useCallback, useState, useEffect} from 'react';
import {Navigation} from '@shopify/polaris';
import {HomeMajorMonotone, OnlineStoreMajorTwotone, OrdersMajorTwotone, ProductsMajorTwotone} from '@shopify/polaris-icons';

import {
  Autocomplete, 
  Button,
  Icon, 
  Card,
  Form,
  FormLayout,
  Layout,
  Page,
  Stack,
  TextField
} from '@shopify/polaris';
import {SearchMinor} from '@shopify/polaris-icons';
import axios from 'axios';
import Cookies from 'js-cookie';

export default function Index() {
  const [clientid, setClientId] = useState('');
  const [clientsecret, setClientSecret] = useState('');

  useEffect(() => {
      axios.post(`http://localhost:3000/get_config`,{
        shop: Cookies.get("shopOrigin"),
      })
      .then(res => {
        setClientId(res.data.client_id);
        setClientSecret(res.data.client_secret);
        // console.log(res);
      })
  }, []);

  const handlerClientId = useCallback((value) => setClientId(value), []);
  const handlerClientSecret = useCallback((value) => setClientSecret(value), []);

  const handleSubmit = useCallback((_event) => {
      axios.post(`http://localhost:3000/save_config`,{
        shop : Cookies.get("shopOrigin"),
        client_id: clientid,
        client_secret: clientsecret,
      })
      .then(res => {
          console.log(res)

      })
  }, [clientid, clientsecret]);

  return (
    <Page>
      <Layout>
        <Layout.AnnotatedSection>
          <Card sectioned>
            <Form onSubmit={handleSubmit}>
              <FormLayout>
                <TextField
                    value={clientid}
                    onChange={handlerClientId}
                    label="Client Id (*)"
                    type="clientid"
                    placeholder="Client Id"
                  />

                <TextField
                    value={clientsecret}
                    onChange={handlerClientSecret}
                    label="Client Secret (*)"
                    type="clientsecret"
                    placeholder="Client Secret"
                  />
              
                <Stack distribution="trailing">
                  <Button primary submit>
                    Save
                  </Button>
                </Stack>
              </FormLayout>
            </Form>
          </Card>
        </Layout.AnnotatedSection>
      </Layout>
    </Page>
  );
}
