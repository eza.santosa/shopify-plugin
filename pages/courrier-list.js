import React, { useState, useEffect, useCallback } from "react";
import { Card, OptionList, Stack, Button, Form, FormLayout} from "@shopify/polaris";
import axios from 'axios';
import Cookies from 'js-cookie';

export default function Courier() {
  const [selected, setSelected] = useState([]);
  const [courier, setCourier] = useState([]);
  const [dataCourier, setDataCourier] = useState([]);
  //console.log(selected)

  useEffect(() => {
    axios.post(`http://localhost:3000/api/list_courier`,{
      shop: Cookies.get("shopOrigin"),
    }).then(res => {
      const data_courier = res.data.list;
      let json = JSON.parse(JSON.stringify(data_courier).split('"code":').join('"value":'));
      json = JSON.parse(JSON.stringify(json).split('"name":').join('"label":'));
      console.log(res.data.list)
      setCourier(json)
    })
    axios.post(`http://localhost:3000/get_courier`,{
      shop: Cookies.get("shopOrigin"),
    })
    .then(res => {
      if(res.data.couriers){
        setSelected(res.data.couriers)
      }
      console.log(res.data.couriers)
    })
  }, []);

  const handleSubmit = useCallback((_event) => {
    axios.post('http://localhost:3000/manage_courier', {
      shop: Cookies.get("shopOrigin"),
      courier: selected
    })
    .then(function (response) {
      console.log(response);
    })
    .catch(function (error) {
      console.log(error);
    });
  }, [selected]);


  return (
    <Card>
      <Form onSubmit={handleSubmit}>
        <FormLayout>
          <OptionList
            title="Manage Activate Courier"
            onChange={setSelected}
            options={courier}
            selected={selected}
            allowMultiple
        />
        <Stack distribution="trailing">
          <Button primary submit>
            Save
          </Button>
        </Stack>
        </FormLayout>
      </Form>
    </Card>
  );
}