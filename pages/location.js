import React, {useCallback, useState, useEffect} from 'react';
import {Navigation} from '@shopify/polaris';
import {HomeMajorMonotone, OnlineStoreMajorTwotone, OrdersMajorTwotone, ProductsMajorTwotone} from '@shopify/polaris-icons';

import {
  Autocomplete, 
  Button,
  Icon, 
  Card,
  Form,
  FormLayout,
  Layout,
  Page,
  Stack,
  TextField
} from '@shopify/polaris';
import {SearchMinor} from '@shopify/polaris-icons';
import axios from 'axios';
import Cookies from 'js-cookie';

export default function Location() {
  const [deselectedOptions, setDeselectedOptions] =  useState([]);
  const [selectedOptions, setSelectedOptions] = useState(deselectedOptions);
  const [address, setAddress] = useState('');
  const [subdistrictCode, setSubdistrictCode] = useState('');
  const [city, setCity] = useState('');
  const [cityCode, setCityCode] = useState('');
  const [province, setProvince] = useState('');
  const [provinceCode, setProvinceCode] = useState('');
  const [location, setLocation] = useState([]);
  const [phoneNumber, setPhoneNumber] = useState('');
  const [zipCode, setZipCode] = useState('');
  const [email, setEmail] = useState('');
  const [note, setNote] = useState('');
  const [inputValue, setInputValue] = useState(deselectedOptions);
  const [options, setOptions] = useState([]);
  const handleChange = useCallback((newValue) => setValue(newValue), []);

  const updateText = useCallback(
    (value) => {
      //console.log(value)
      setInputValue(value);

      if (value === '') {
        setOptions(deselectedOptions);
        return;
      }

      const filterRegex = new RegExp(value, 'i');
      const resultOptions = deselectedOptions.filter((option) =>
        option.label.match(filterRegex),
      );
      setOptions(resultOptions);
    },
    [deselectedOptions],
  );

  const updateSelection = useCallback((selected) => {
    const data_loc = selected.toString().split("/");
    const data_sub =  data_loc[0].split('-');
    const data_city =  data_loc[1].split('-');
    console.log(data_city);
    const data_prov =  data_loc[2].split('-');
    setSubdistrictCode(data_sub[0]);
    setInputValue(data_sub[1]);
    setCityCode(data_city[0]);
    setCity(data_city[1]);
    setProvinceCode(data_prov[0]);
    setProvince(data_prov[1]);
  }, []);

  const textField = (
    <Autocomplete.TextField
      onChange={updateText}
      label="Subdistrict"
      value={inputValue}
      prefix={<Icon source={SearchMinor} color="inkLighter" />}
      placeholder="Search"
    />
  );

  useEffect(() => {
    axios.get(`http://localhost:3000/master_locations`)
      .then(res => {
        const datax = [];
        res.data.forEach(function (element) {
          element.value = element.subdistrict_code+'-'+ element.subdistrict_name+'/'+ element.city_code+'-'+ element.city_name+'/'+element.province_code+'-'+ element.province_name;
          element.label = element.subdistrict_name+' - ' + element.city_name;
        });
        setDeselectedOptions(res.data);
      
    })

    axios.post(`http://localhost:3000/get_location`,{
      shop: Cookies.get("shopOrigin"),
    })
    .then(res => {
        setAddress(res.data.address);
        setPhoneNumber(res.data.phone_number);
        setInputValue(res.data.subdistrict_name);
        setSubdistrictCode(res.data.subdistrict_code);
        setCityCode(res.data.city_code);
        setCity(res.data.city_name);
        setProvinceCode(res.data.province_code);
        setProvince(res.data.province_name);
        setZipCode(res.data.zip_code);
        setEmail(res.data.email);
        setNote(res.data.note);
    })
  }, []);

  const handleAddress = useCallback((value) => setAddress(value), []);
  const handlerPhoneNumber = useCallback((value) => setPhoneNumber(value), []);
  const handlerZipcode = useCallback((value) => setZipCode(value), []);
  const handlerEmail = useCallback((value) => setEmail(value), []);
  const handlerNote = useCallback((value) => setNote(value), []);

  const handleSubmit = useCallback((_event) => {
      console.log(inputValue);
      axios.post(`http://localhost:3000/save_location`,{
        shop : Cookies.get("shopOrigin"),
        phone_number: phoneNumber,
        address: address,
        zip_code: zipCode,
        subdistrict_code: subdistrictCode,
        subdistrict_name: inputValue,
        city_code: cityCode,
        city_name: city,
        province_code: provinceCode,
        province_name: province,
        email: email,
        note: note
      })
      .then(res => {
          console.log(res)

      })
  }, [address, city, province, phoneNumber, zipCode, email, note]);

  return (
    <Page>
      <Layout>
        <Layout.AnnotatedSection>
          <Card sectioned>
            <Form onSubmit={handleSubmit}>
              <FormLayout>
                <TextField
                    value={address}
                    onChange={handleAddress}
                    label="Adress (*)"
                    type="address"
                    placeholder="Address"
                  />
                <Autocomplete
                  options={options}
                  selected={selectedOptions}
                  onSelect={updateSelection}
                  textField={textField}
                />
                <TextField
                  value={city}
                  label="City (*)"
                  disabled
                />
                <TextField
                  value={province}
                  label="Province (*)"
                  disabled
                />
                <TextField
                  value={zipCode}
                  onChange={handlerZipcode}
                  label="Zip Code (*)"
                  type="number"
                  placeholder="Zip Code"
                />
                <TextField
                  value={phoneNumber}
                  onChange={handlerPhoneNumber}
                  label="Phone Number (*)"
                  placeholder="Phone Number"
                />
                <TextField
                  value={email}
                  onChange={handlerEmail}
                  label="Email"
                  type="email"
                  placeholder="Email"
                />
                <TextField
                  value={note}
                  onChange={handlerNote}
                  label="Note"
                  type="note"
                  placeholder="Note"
                />
                <Stack distribution="trailing">
                  <Button primary submit>
                    Save
                  </Button>
                </Stack>
              </FormLayout>
            </Form>
          </Card>
        </Layout.AnnotatedSection>
      </Layout>
    </Page>
  );
}
