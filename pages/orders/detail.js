import React from 'react';
import {Badge, Button, Card, Heading , Layout ,Modal, Page, ResourceList, Select, TextContainer, TextStyle, Thumbnail} from '@shopify/polaris';
import {EditMinor, PrintMinor, CircleDisableMinor, CancelSmallMinor, CircleTickOutlineMinor } from '@shopify/polaris-icons';
import axios from 'axios';
import Cookies from 'js-cookie';

export default class App extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      active: false,
      active2: false,
      active3: false,
      activeConfirm: false,
      delivertype: '',
      status: '',
      name: '',
      id: '',
      shipdeo_id: '',
      sub_total: 0,
      total_price: 0,
      line_items: [],
      billing_address: [],
      shipping_address: [],
      airwaybill: '',
      data_waybill: [],
      courier: '',
      data_origin: [],
      financial_status: '',
      courier_name: '',
      tracking_number: '',
      shipper_name: '',
      receiver_name: '',
      origin: '',
      destination: '',
    };
    this.handleCancelOrder = this.handleCancelOrder.bind(this)
    this.handleCancelShipping = this.handleCancelShipping.bind(this)
    this.handleConfirm = this.handleConfirm.bind(this)
    this.handleChange = this.handleChange.bind(this)
    this.handleChangeShipping = this.handleChangeShipping.bind(this)
    this.handleChangeConfirm = this.handleChangeConfirm.bind(this)
    this.handleTracking = this.handleTracking.bind(this)
    this.handleDeliveryType = this.handleDeliveryType.bind(this)
  }


  componentDidMount() {
    const queryString = window.location.search;
    const urlParams = new URLSearchParams(queryString);
    const val_name = urlParams.get('name');
    const val_id = urlParams.get('id');
    const val_shipdeo_id = urlParams.get('shipdeo_id')
    axios.post(`http://localhost:3000/detail_order`,{
      shop: Cookies.get("shopOrigin"),
      order_id: val_id,
      shipdeo_id: val_shipdeo_id
    })
    .then(res => {
        // console.log(res.data);
        if(res.data){
          this.setState({
            financial_status: res.data.shopify.financial_status,
            sub_total: res.data.shopify.subtotal_price,
            total_price: res.data.shopify.total_price,
            line_items: res.data.shopify.line_items,
            billing_address: res.data.shopify.billing_address,
            shipping_address: res.data.shopify.shipping_address,
            airwaybill: res.data.shipdeo.tracking_info.airwaybill,
            courier: res.data.shipdeo.courier,
            data_origin: res.data.shipdeo.origin,
            status: res.data.shipdeo.status
          })
          console.log(res.data.shipdeo.tracking_info.airwaybill)
          if(res.data.shipdeo.tracking_info.airwaybill) {
            axios.post(`http://localhost:3000/get_waybill`,{
              shop: Cookies.get("shopOrigin"),
              courier: this.state.courier,
              airwaybill: this.state.airwaybill,
            })
            .then(res => {
              this.setState({
                courier_name: res.data.data.summary.courier_name,
                tracking_number: res.data.data.summary.tracking_number,
                shipper_name: res.data.data.summary.shipper_name,
                receiver_name: res.data.data.summary.receiver_name,
                origin: res.data.data.summary.origin,
                destination: res.data.data.summary.destination,
                data_waybill : res.data.data.outbounds
              })
              console.log(this.state.data_waybill)
            });
          }
        
        }
    })
    
    this.setState({
        name : '#'+val_name,
        id : val_id,
        shipdeo_id: val_shipdeo_id
    })
    // console.log(params[1]);
  }

  handleCancelOrder() {
    console.log(this.state.status)
    this.setState(prevState => ({
      active: !prevState.active
    }));
  }

  handleCancelShipping() {
    this.setState(prevState => ({
      active2: !prevState.active2
    }));
  }

  handleConfirm() {
    this.setState(prevState => ({
      activeConfirm: !prevState.activeConfirm
    }));
  }



  handleTracking() {
    this.setState(prevState => ({
      active3: !prevState.active3
    }));
  }



  handleChange() { 
    if(this.state.status == 'ENTRY' || this.state.status == 'CONFIRMED'){
      axios.post(`http://localhost:3000/cancel_order`,{
        shop: Cookies.get("shopOrigin"),
        order_id: this.state.id,
      })
      .then(res => {
          console.log(res.data);
        
      })
    }else{
      alert('Order Shopify tidak bisa dicancel karena sudah dipicked!');
    }
  }

  handleChangeShipping() {
    if(this.state.status == 'ENTRY' || this.state.status == 'CONFIRMED'){
      axios.post(`http://localhost:3000/cancel_shipping`,{
        shop: Cookies.get("shopOrigin"),
        shipdeo_id: this.state.shipdeo_id,
      })
      .then(res => {
          console.log(res.data);
      })

    }else{
      alert('Shipping tidak bisa dicancel karena sudah dipicked!');
    }
    
  }

  handleChangeConfirm() {
    if(this.state.status == 'ENTRY'){
      axios.post(`http://localhost:3000/confirm_order`,{
        shop: Cookies.get("shopOrigin"),
        order_id: this.state.id,
        shipdeo_id: this.state.shipdeo_id,
        data_origin: this.state.data_origin,
        deliver_type : this.state.delivertype
  
      })
      .then(res => {
          console.log(res.data);
        
      })
    }else{
      alert('Order sudah di confirm!');
    }
   
  }

  handleDeliveryType (e) {
    this.setState({delivertype: e})
    console.log(e)
  }


  render() {
    const deliveryTypeList = [
      {label: 'Pickup', value: 'pickup'},
      {label: 'Dropoff', value: 'dropoff'},
    ];

    return (
      <Page
          breadcrumbs={[{content: 'Orders', url: '/orders'}]}
          title={this.state.name}
          titleMetadata={<Badge status="warning">{this.state.financial_status}</Badge>}
          secondaryActions={[
            {content: 'Print', icon: PrintMinor},
            {content: 'Edit', icon: EditMinor, url: '/orders/edit?id='+ this.state.id +'&shipdeo_id=' + this.state.shipdeo_id },
            {content: 'Cancel order', icon: CancelSmallMinor, onAction: this.handleCancelOrder, },
            {content: 'Cancel shipping', icon: CircleDisableMinor, onAction: this.handleCancelShipping, },
            {content: 'Confirm', icon: CircleTickOutlineMinor, onAction: this.handleConfirm, },
          ]}
          pagination={{
            hasPrevious: true,
            hasNext: true,
          }}
        >
        <Modal
          open={this.state.active}
          onClose={this.handleCancelOrder}
          title="Cancel Order"
          primaryAction={{
            content: 'Cancel',
            onAction: this.handleChange,
          }}
        >
          <Modal.Section>
            <TextContainer>
              <p>
               Apakah anda yakin akan membatalkan Order ini?
              </p>
            </TextContainer>
          </Modal.Section>
        </Modal>
        <Modal
          open={this.state.active2}
          onClose={this.handleCancelShipping} 
          title="Cancel Shipping"
          primaryAction={{
            content: 'Cancel',
            onAction: this.handleChangeShipping,
          }}
        >
          <Modal.Section>
            <TextContainer>
              <p>
               Apakah anda yakin akan membatalkan Shippping ini?
              </p>
            </TextContainer>
          </Modal.Section>
        </Modal>
        <Modal
          open={this.state.active3}
          title="Tracking Waybilll"
          onClose={this.handleTracking}
        >   
           <Modal.Section>
            <TextContainer>
                <p>Courier Name: {this.state.courier_name}</p>
                <p>Tracking Number:  {this.state.tracking_number}</p>
                <p>Pengirin: {this.state.shipper_name}</p>
                <p>Penerima : {this.state.receiver_name}</p>
                <p>Asal: {this.state.origin}</p>
                <p>Tujuan: {this.state.destination}</p>
                <p>Status: {this.state.status}</p>
            </TextContainer>
          </Modal.Section>
           {this.state.data_waybill.map((waybill, j) =>
            <Modal.Section key={j}>
              <TextContainer key={j}>
                <Heading>{waybill.outbound_date}</Heading>
                <p key={j}>
                  {waybill.outbound_description}
                </p>
              </TextContainer>
            </Modal.Section>
           )}
        </Modal>
        <Modal
           open={this.state.activeConfirm}
           onClose={this.handleConfirm}
           title="Confirm Order"
           primaryAction={{
             content: 'Confirm',
             onAction: this.handleChangeConfirm,
           }}
         >
           <Modal.Section>
             <TextContainer>
               <p>
                Apakah anda yakin akan melakukan Confirm Order?
               </p>
               <Select
                  label="Select Delivery Type"
                  placeholder="Select"
                  options={deliveryTypeList}
                  onChange={this.handleDeliveryType}
                  value={this.state.delivertype}
                />
             </TextContainer>
           </Modal.Section>
         </Modal>
        <Layout>
          <Layout.Section>
            <Card
              title="Order Detail"
              primaryFooterAction={{
                content: 'View tracking number',
                onAction: this.handleTracking,
              }}
              sectioned
            >
              <Card.Section title="Items">
              {this.state.line_items.map((item, i) =>
                <div>
                  {/* <Thumbnail
                    key="tes"
                    source="https://burst.shopifycdn.com/photos/black-leather-choker-necklace_373x@2x.jpg"
                    alt="Black choker necklace"
                  /> */}
                  <p key='item'>{item.name}</p>
                  <p key="sub_total">Sub Total : {this.state.sub_total}</p>
                  <p key="total">Total : {this.state.total_price}</p>
                </div>
              )}
              </Card.Section>
            </Card>
          </Layout.Section>
          <Layout.Section secondary>
            <Card title="Information">
              <Card.Section title="Billling">
                  <div>
                    <p key='name'>{this.state.billing_address.name}</p>
                    <p key='phone'>{this.state.billing_address.phone}</p>
                    <p key='address1'>{this.state.billing_address.address1}</p>
                    <p key='address2'>{this.state.billing_address.address2}</p>
                    <p key='city'>{this.state.billing_address.city}</p>
                    <p key='province'>{this.state.billing_address.province} {this.state.billing_address.zip}</p>
                    <p key='country'>{this.state.billing_address.country}</p>
                  </div>
                
              </Card.Section>
              <Card.Section title="Shipping">
                  <div>
                    <p key='name'>{this.state.shipping_address.name}</p>
                    <p key='phone'>{this.state.shipping_address.phone}</p>
                    <p key='address1'>{this.state.shipping_address.address1}</p>
                    <p key='address2'>{this.state.shipping_address.address2}</p>
                    <p key='city'>{this.state.shipping_address.city}</p>
                    <p key='province'>{this.state.shipping_address.province} {this.state.shipping_address.zip}</p>
                    <p key='country'>{this.state.shipping_address.country}</p>
                  </div>
              </Card.Section>
            </Card>
          </Layout.Section>
        </Layout>
      </Page>
    );
  }
}
