import React, {useCallback, useState, useEffect, useRef, useLayoutEffect} from 'react';
import {Navigation} from '@shopify/polaris';
import {HomeMajorMonotone, OnlineStoreMajorTwotone, OrdersMajorTwotone, ProductsMajorTwotone} from '@shopify/polaris-icons';

import {
  Autocomplete, 
  Button,
  Icon, 
  Card,
  ChoiceList, 
  Form,
  FormLayout,
  Layout,
  Page,
  Select,
  Stack,
  TextField
} from '@shopify/polaris';
import {SearchMinor} from '@shopify/polaris-icons';
import axios from 'axios';
import Cookies from 'js-cookie';

export default function Location() {
  const [deselectedOptions, setDeselectedOptions] =  useState([]);
  const [selectedOptions, setSelectedOptions] = useState(deselectedOptions);
  const [firstName, setFirstName] = useState('');
  const [lastName, setLastName] = useState('');
  const [address, setAddress] = useState('');
  const [subdistrictCode, setSubdistrictCode] = useState('');
  const [city, setCity] = useState('');
  const [cityCode, setCityCode] = useState('');
  const [province, setProvince] = useState('');
  const [provinceCode, setProvinceCode] = useState('');
  const [location, setLocation] = useState([]);
  const [phoneNumber, setPhoneNumber] = useState('');
  const [zipCode, setZipCode] = useState('');
  const [email, setEmail] = useState('');
  const [note, setNote] = useState('');
  const [inputValue, setInputValue] = useState(deselectedOptions);
  const [options, setOptions] = useState([]);
  const [item, setItem] = useState([]);
  const [dataItem, setDataItem] = useState(item);
  const [selected, setSelected] = useState('');
  const [list, setList] = useState([]);
  const [deliveryType, setDeliveryType] = useState('');
  const [payment, setPayment] = useState('');
  const [listShipping, setListShipping] = useState('')
  const itemRef = useRef();
  const transactionRef = useRef();
  const zipRef = useRef();
  const shippingRef = useRef();
  const idRef = useRef();
  const shipdeoIdRef = useRef();


  useEffect(() => {
    const queryString = window.location.search;
    const urlParams = new URLSearchParams(queryString);
    const id = urlParams.get('id');
    const shipdeo_id = urlParams.get('shipdeo_id');
    
    idRef.current = id;
    shipdeoIdRef.current = shipdeo_id;
  

    axios.get(`http://localhost:3000/master_locations`)
      .then(res => {
        const datax = [];
        res.data.forEach(function (element) {
          element.value = element.subdistrict_code+'-'+ element.subdistrict_name+'/'+ element.city_code+'-'+ element.city_name+'/'+element.province_code+'-'+ element.province_name;
          element.label = element.subdistrict_name+' - ' + element.city_name;
        });
        setDeselectedOptions(res.data);
    })

    axios.post(`http://localhost:3000/edit_order`,{
      shop: Cookies.get("shopOrigin"),
      order_id: id,
      shipdeo_id: shipdeo_id
    })
    .then(res => {
        const fullnames = res.data.destination.contact.name;
        let names = fullnames.split(' ');
        itemRef.current = res.data.items;
        transactionRef.current = res.data.transaction;
        zipRef.current = res.data.destination.postal_code;
        setFirstName(names[0]);
        console.log(names)
        if(names.length > 1) {
          let last_name = '';
          for (var i = 1; i < names.length; i++) {
            last_name += names[i] + ' ';
          }
          setLastName(last_name);
        }
        setAddress(res.data.destination.contact.address);
        setPhoneNumber(res.data.destination.contact.phone);
        setInputValue(res.data.destination.subdistrict_name);
        setSubdistrictCode(res.data.destination.subdistrict_code);
        setCityCode(res.data.destination.city_code);
        setCity(res.data.destination.city_name);
        setProvinceCode(res.data.destination.province_code);
        setProvince(res.data.destination.province_name);
        setZipCode(res.data.destination.postal_code);
        setEmail(res.data.destination.contact.email);
        setNote(res.data.destination.note);
        setItem(res.data.items)
    })

  }, []);

  const updateText = useCallback(
    (value) => {
      setInputValue(value);

      if (value === '') {
        setOptions(deselectedOptions);
        return;
      }

      const filterRegex = new RegExp(value, 'i');
      const resultOptions = deselectedOptions.filter((option) =>
        option.label.match(filterRegex),
      );
      setOptions(resultOptions);
    },
    [deselectedOptions],
  );

  const updateSelection = useCallback((selected) => {
    const dataItem = itemRef.current;
    const zip_code = zipRef.current;
    const data_loc = selected.toString().split("/");
    const data_sub =  data_loc[0].split('-');
    const data_city =  data_loc[1].split('-');
    const data_prov =  data_loc[2].split('-');
    setSubdistrictCode(data_sub[0]);
    setInputValue(data_sub[1]);
    setCityCode(data_city[0]);
    setCity(data_city[1]);
    setProvinceCode(data_prov[0]);
    setProvince(data_prov[1]);
    
    axios.post(`http://localhost:3000/get_shipping`,{
      method: 'edit',
      shop: Cookies.get("shopOrigin"),
      subdistrict_code: data_sub[0],
      subdistrict_name: data_sub[1],
      city_code: data_city[0],
      city_name: data_city[1],
      zip_code: zip_code,
      item: dataItem
    })
    .then(res => {
      const datax =  [];
      console.log(res);
      for (const item of res.data.data) {
        datax.push({
          label: item.courier + '-' + item.service + ' | ' + item.duration + ' ( Rp.' + item.price + ')',
          value: item.courierCode + '/' + item.service + '/' + item.price,
        })
      }
      setListShipping('List Kurir');
      setList(datax);  
    })

  }, []);

  const textField = (
    <Autocomplete.TextField
      onChange={updateText}
      label="Subdistrict"
      value={inputValue}
      prefix={<Icon source={SearchMinor} color="inkLighter" />}
      placeholder="Search"
    />
  );

  const handleFirstName = useCallback((value) => setFirstName(value), []);
  const handleLastName = useCallback((value) => setLastName(value), []);
  const handleAddress = useCallback((value) => setAddress(value), []);
  const handlerPhoneNumber = useCallback((value) => setPhoneNumber(value), []);
  const handlerZipcode = useCallback((value) => setZipCode(value), []);
  const handlerEmail = useCallback((value) => setEmail(value), []);
  const handlerNote = useCallback((value) => setNote(value), []);
  const handleDeliveryType = useCallback((value) => setDeliveryType(value), []);
  const handlePayment = useCallback((value) => {
    console.log(value);
    setPayment(value) 
  }, []);
  const handleChange = useCallback((value) =>  {
  shippingRef.current = value.toString();
  setSelected(value)} ,[]);


  const handleSubmit = useCallback((_event) => {
      const data_shiping = shippingRef.current.split('/')
      transactionRef.current['shipping_charge'] = Number.parseInt(data_shiping[2], 10);
      transactionRef.current['total_value'] =  transactionRef.current['subtotal'] + transactionRef.current['shipping_charge'];
      transactionRef.current['total_cod'] =  transactionRef.current['subtotal'] + transactionRef.current['shipping_charge'];
      let isCod = false;
      if(payment == 'cod'){
        isCod = true;
      }
      var today = new Date();
      var date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
      var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();

      axios.post(`http://localhost:3000/save_order`,{
        method: 'edit',
        shop : Cookies.get("shopOrigin"),
        id: idRef.current,
        shipdeo_id : shipdeoIdRef.current,
        first_name: firstName,
        last_name: lastName,
        courier: data_shiping[0],
        tenant_id: 1114, 
        courier_service: data_shiping[1],
        phone: phoneNumber,
        address: address,
        postal_code: zipRef.current,
        subdistrict_code: subdistrictCode,
        subdistrict_name: inputValue,
        city_code: cityCode,
        city_name: city,
        province_code: provinceCode,
        province_name: province,
        email: email,
        note: note,
        items: itemRef.current,
        transaction: transactionRef.current,
        delivery_type: deliveryType,
        delivery_time: date+' '+time, 
        payment: payment
      })
      .then(res => {
          console.log(res)

      })
  }, [address, city, province, phoneNumber, zipCode, email, note, deliveryType, payment]);

  
  const deliveryTypeList = [
    {label: 'Pickup', value: 'pickup'},
    {label: 'Dropoff', value: 'dropoff'},
  ];

  const paymentMethod = [
    {label: 'COD', value: 'cod'},
    {label: 'non-COD', value: 'non-cod'},
  ];
  
  return (
    <Page>
      <Layout>
        <Layout.AnnotatedSection
            title="Account details"
            description="Shipdeo will use this as your account information."
          >
          <Card sectioned>
            <Form onSubmit={handleSubmit}>
              <FormLayout>
                <FormLayout.Group condensed>
                  <TextField
                    value={firstName}
                    onChange={handleFirstName}
                    label="First Name (*)"
                    type="firstname"
                    placeholder="First Name"
                  />
                  <TextField
                    value={lastName}
                    onChange={handleLastName}
                    label="Last Name (*)"
                    type="lastname"
                    placeholder="Last Name"
                  />
                </FormLayout.Group>
                <TextField
                    value={address}
                    onChange={handleAddress}
                    label="Adress (*)"
                    type="address"
                    placeholder="Address"
                  />
                <Autocomplete
                  options={options}
                  selected={selectedOptions}
                  onSelect={updateSelection}
                  textField={textField}
                />
                <TextField
                  value={city}
                  label="City (*)"
                  disabled
                />
                <TextField
                  value={province}
                  label="Province (*)"
                  disabled
                />
                <TextField
                  value={zipCode}
                  onChange={handlerZipcode}
                  label="Zip Code (*)"
                  type="number"
                  placeholder="Zip Code"
                />
                <TextField
                  value={phoneNumber}
                  onChange={handlerPhoneNumber}
                  label="Phone Number (*)"
                  placeholder="Phone Number"
                />
                <TextField
                  value={email}
                  onChange={handlerEmail}
                  label="Email"
                  type="email"
                  placeholder="Email"
                />
                <TextField
                  value={note}
                  onChange={handlerNote}
                  label="Note"
                  type="note"
                  placeholder="Note"
                />
                <TextField
                  value={note}
                  onChange={handlerNote}
                  label="Note"
                  type="note"
                  placeholder="Note"
                  hidden
                />
                <ChoiceList
                  title={listShipping}
                  choices={list}
                  selected={selected}
                  onChange={handleChange}
                />

                <Select
                  label="Delivery Type"
                  placeholder="Select"
                  options={deliveryTypeList}
                  onChange={handleDeliveryType}
                  value={deliveryType}
                /> 

                <Select
                  label="Payment Method"
                  placeholder="Select"
                  options={paymentMethod}
                  onChange={handlePayment}
                  value={payment}
                />
              
                <Stack distribution="trailing">
                  <Button primary submit>
                    Save
                  </Button>
                </Stack>
              </FormLayout>
            </Form>
          </Card>
        </Layout.AnnotatedSection>
      </Layout>
    </Page>
  );
}
