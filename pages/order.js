import React, {useCallback, useState, useEffect} from 'react';
import {Card, DataTable, Link, Page} from '@shopify/polaris';
import axios from 'axios';
import Cookies from 'js-cookie';

export default function FullDataTableExample() {
  const [sortedRows, setSortedRows] = useState(null);
  const [row, setRow] =  useState([]);


  useEffect(() => {
    axios.post(`http://localhost:3000/get_order`,{
      shop: Cookies.get("shopOrigin"),
    })
    .then(res => {
      console.log(res);
      const datax = [];
      for(const row of res.data){
        // console.log(row.line_items);
        const link =  '/orders/detail?id=' + row.order_id+'&shipdeo_id='+row.order_id_shipdeo+'&name='+row.name.substr(1);
        datax.push({
          name: <Link url={link} key="emerald-silk-gown">{row.name}</Link>,
          date: row.created_at,
          airwaybill: row.airwaybill,
          total_price: row.total_price,
          payment: row.financial_status,
          fulfillment: row.fulfillment_status,
          qty: ''
        })
        // console.log(row);
        // datax.push(row)
      }
      let array = datax.map(obj => Object.values(obj));
      // console.log(array);
      // if(res.data){
      //   setRow(datax)
      // }else{
        setRow(array)
      //}
      // console.log(datax);
      // setRow([])
      // console.log(res);
    })
  }, []);

  const initiallySortedRows = row;

  const rows = sortedRows ? sortedRows : initiallySortedRows;
  const handleSort = useCallback(
    (index, direction) => setSortedRows(sortCurrency(rows, index, direction)),
    [rows],
  );

  return (
    <Page title="Order">
      <Card>
        <DataTable
          columnContentTypes={[
            'text',
            'text',
            'text',
            'numeric',
            'text',
            // 'text',
            // 'numeric',
          ]}
          headings={[
            'Order',
            'Date',
            'Airwaybill',
            'Total',
            'Payment',
            // 'Fulfillment',
            // 'Qty',
          ]}
          rows={rows}
          // totals={['', '', '', 255, '$155,830.00']}
          sortable={[false, true, false, false, true]}
          defaultSortDirection="descending"
          initialSortColumnIndex={4}
          onSort={handleSort}
          footerContent={`Showing ${rows.length} of ${rows.length} results`}
        />
      </Card>
    </Page> 
  );

  function sortCurrency(rows, index, direction) {
    return [...rows].sort((rowA, rowB) => {
      const amountA = parseFloat(rowA[index].substring(1));
      const amountB = parseFloat(rowB[index].substring(1));

      return direction === 'descending' ? amountB - amountA : amountA - amountB;
    });
  }
}
