var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var featureSchema = new Schema({
   shop:String,
   features: [{
      type: String
    }]
});
module.exports =  mongoose.model('feature', featureSchema, 'features'); 