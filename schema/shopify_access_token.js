var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var tokenSchema = new Schema({
   shop:String,
   token:String,
});
module.exports =  mongoose.model('shopify_access_token', tokenSchema, 'shopify_access_token');