var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var orderSchema = new Schema({
   order_id: String,
   order_id_shipdeo: String,
   shop: String,
   name: String,
   created_at: String,
   updated_at: String,
   financial_status: String,
   fulfillment_status: String,
   item: String,
   total_price: String,
   courier: String,
   service: String,
   order_number: String,
   airwaybill : String,
   booking_code: String,
});
module.exports =  mongoose.model('order', orderSchema, 'orders');