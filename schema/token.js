var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var tokenSchema = new Schema({
   shop:String,
   access_token:String,
});
module.exports =  mongoose.model('token', tokenSchema, 'tokens');