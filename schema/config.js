var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var configSchema = new Schema({
   shop: String,
   client_id: String,
   client_secret: String,
});
module.exports =  mongoose.model('config', configSchema, 'configs');