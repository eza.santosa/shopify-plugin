var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var storeLocationSchema = new Schema({
   shop: String,
   phone_number: String,
   address: String,
   zip_code: String,
   subdistrict_code: String,
   subdistrict_name: String,
   city_code: String,
   city_name: String,
   province_code: String,
   province_name: String,
   email: String,
   note: String,
   bank_account: Array
});

module.exports =  mongoose.model('StoreLocations', storeLocationSchema, 'store_locations');