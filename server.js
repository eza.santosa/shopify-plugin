require('isomorphic-fetch');
const Koa = require('koa');
const BodyParser = require("koa-bodyparser");
const Router = require('koa-router');
const cors = require('koa-cors');
const HttpStatus = require("http-status");
const next = require('next');
const { default: createShopifyAuth } = require('@shopify/koa-shopify-auth');
const dotenv = require('dotenv');
const { verifyRequest } = require('@shopify/koa-shopify-auth');
const session = require('koa-session');
const mongoose = require('mongoose');
const axios = require('axios');
var querystring = require('querystring');
const serve = require('koa-static');
var Pug = require('koa-pug');
var $ = require("jquery");
const Token = require('./schema/token.js');
const Courier = require('./schema/courier.js');
const Features = require('./schema/feature.js');
const Subdistrict = require('./schema/subdistrict.js');
const StoreLocations = require('./schema/store_locations.js');
const ShopifyAccesToken = require('./schema/shopify_access_token.js');
const Order = require('./schema/order.js');
const Config = require('./schema/config.js');
const OrderLog = require('./schema/order_logs.js');
const { data } = require('jquery');
const order = require('./schema/order.js');

dotenv.config();

const port = parseInt(process.env.PORT, 10) || 3000;
const dev = process.env.NODE_ENV !== 'production';
const app = next({ dev });
const handle = app.getRequestHandler();
const { SHOPIFY_API_SECRET_KEY, SHOPIFY_API_KEY } = process.env;

app.prepare().then(() => {
  const server = new Koa();
  const router = new Router();
  server.use(BodyParser());
  server.use(cors());
  server.use(session({ sameSite: 'none', secure: true }, server));
  server.keys = [SHOPIFY_API_SECRET_KEY];
  server.use(serve('./public'));
  var pug = new Pug({
    viewPath: './views',
    basedir: './views',
    app: server //Equivalent to app.use(pug)
 });

  server.use(
    createShopifyAuth({
      apiKey: SHOPIFY_API_KEY,
      secret: SHOPIFY_API_SECRET_KEY,
      accessMode: 'offline',
      scopes: [
          'read_orders',
          'write_orders',
          'read_products',
          'write_products',
          'read_themes',
          'write_themes',
          'read_shipping',
          'write_shipping',
          'read_script_tags', 
          'write_script_tags',
          'read_locations',
          'read_inventory', 
          'write_inventory',
        ],
       afterAuth(ctx) {
        console.log(ctx)
        const { shop, accessToken } = ctx.session;
       // token = accessToken;
        console.log(accessToken);
        ctx.cookies.set("shopOrigin", shop, {
          httpOnly: false,
          secure: true,
          sameSite: 'none'
        });
        ctx.cookies.set("accessToken", accessToken, {
          httpOnly: false,
          secure: true,
          sameSite: 'none'
        });
        saveAccessToken(shop,accessToken);
        ctx.redirect('/');
      },
    }),
  );
  
  //server.use(graphQLProxy({version: ApiVersion.October19}))
  // server.use(verifyRequest());
  // server.use(async (ctx) => {
  //   await handle(ctx.req, ctx.res);
  //   ctx.respond = false;
  //   ctx.res.statusCode = 200;
  // });

  function saveAccessToken(shop, access_token){
    mongoose.connect('mongodb://localhost:27017/shopify',{ useNewUrlParser: true, useUnifiedTopology: true });
    mongoose.set('useFindAndModify', false);
    const db = mongoose.connection;
    db.on('error', console.error.bind(console, 'connection error:'));
    db.once('open', function() {
        data_token = { 
          shop: shop, 
          token: access_token, 
        }
        console.log(data_token);
        var save_token = new ShopifyAccesToken(data_token);
        var query = { shop: shop };
        const data = ShopifyAccesToken.findOneAndUpdate(query ,data_token,(error, doc) => {
          if(doc == null) {
            save_token.save(function (err, token) {
              if (err) return console.error(err);
              console.log(token.shop + " saved to collection.");
            });
          }
        });
    });
  }

  async function generateToken(param){
    console.log(param);
    mongoose.connect('mongodb://localhost:27017/shopify',{ useNewUrlParser: true, useUnifiedTopology: true });
    const config = await Config.findOne({shop: param}, function (err, res) {
      return res;
    });
    console.log(config);
    const data = {
      grant_type: 'client_credentials',
      client_id: config.client_id,
      client_secret: config.client_secret,
    };
    const res = await axios.post('https://auth-api-development.shipdeo.com/oauth2/connect/token', querystring.stringify(data));
    return res;
  }

  async function getToken(param){
    mongoose.connect('mongodb://localhost:27017/shopify',{ useNewUrlParser: true, useUnifiedTopology: true });
    const token = await Token.findOne({shop: param}, function (err, res) {
      return res;
    });
    if(token) {
      const headers = {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + token.access_token,
      }
      // console.log(headers);
      const results = await axios.get("https://main-api-development.shipdeo.com/v1/couriers", {
        headers: headers,
      }).then(response => { 
          console.log(response);
          return response.status;
      })
      .catch(error => {
          return error.response;
      });
      if(results === 200) {
        return token;
      }else{
        res_token = await generateToken(param);
        const doc = await Token.findOneAndUpdate({shop:param}, {access_token: res_token.data.access_token});
        return res_token.data;

      }
    }else{
      const res = await generateToken(param);
      const data_token = {
          shop: param, 
          access_token: res.data.access_token, 
      }
      const save_token = new Token(data_token);
      return await save_token.save();
    }
  }

  async function getLocation(param){
    mongoose.connect('mongodb://localhost:27017/shopify',{ useNewUrlParser: true, useUnifiedTopology: true });
    const data = await StoreLocations.findOne( { shop: param },{},function (err, res) {
      return res;
    })
    return data;

  }

  async function getShopify(param){
    const shopify = await ShopifyAccesToken.findOne({ shop: param.shop },{},function (err, res) {
      return res;
    });
    const headers = {
      'Content-Type': 'application/json',
      'X-Shopify-Access-Token': shopify.token,
    }
    const results = await axios.get('https://'+param.shop+"/admin/api/2020-07/orders/"+param.order_id + ".json",
      {headers: headers});
    return results;

  }


  async function getLocationShopify(param){
    console.log(param);
    const shopify = await ShopifyAccesToken.findOne({ shop: param },{},function (err, res) {
      return res;
    });
    console.log(shopify)
    const headers = {
      'Content-Type': 'application/json',
      'X-Shopify-Access-Token': shopify.token,
    }
    const results = await axios.get('https://'+param+"/admin/api/2020-07/locations.json",
      {headers: headers})
      .then(response => { 
        //console.log(response);
        return response.data;
      })
      .catch(error => {
        //console.log(error);
        return error.response;
      });
    // console.log(results);
    return results;

  }


  async function saveOrderToShipdeo(datax, shop_name){
    console.log(datax);
    const origin = await getLocation(shop_name).then(data => {
      return data;
    })
    let isCod = false;
    let deliveryType = '';
    if(datax.method == 'edit'){
      deliveryType = datax.delivery_type;
    }else{
      deliveryType = 'pickup';
    }
    if(datax.payment === 'cod'){
      isCod = true;
    }
    const body = {
      courier: datax.courier,
      tenant_id: 1114, 
      courier_service: datax.courier_service,
      is_cod: isCod,
      delivery_type: deliveryType,
      delivery_time: datax.delivery_time,
      origin_lat: "",
      origin_long: "",
      origin_subdistrict_code: origin.subdistrict_code,
      origin_subdistrict_name: origin.subdistrict_name,
      origin_city_code: origin.city_code,
      origin_city_name: origin.city_name,
      origin_province_code: origin.province_code,
      origin_province_name: origin.province_name,
      origin_contact_name: origin.shop,
      origin_contact_phone: origin.phone_number,
      origin_contact_address: origin.address,
      origin_contact_email: origin.email,
      origin_note: origin.note,
      origin_postal_code: origin.zip_code,
      destination_lat: "",
      destination_long: "",
      destination_subdistrict_code: datax.subdistrict_code,
      destination_subdistrict_name: datax.subdistrict_name,
      destination_city_code: datax.city_code,
      destination_city_name: datax.city_name,
      destination_province_code: datax.province_code,
      destination_province_name: datax.province_name,
      destination_postal_code: datax.postal_code,
      destination_contact_name: datax.first_name + ' ' + datax.last_name,
      destination_contact_phone: datax.phone,
      destination_contact_address: datax.address,
      destination_contact_email: datax.email,
      destination_note: datax.note,
      delivery_note: "",
      dimension_uom: "cm",
      pickup_merchant_code: "",
      pickup_merchant_name: "",
      items: datax.items,
      transaction: datax.transaction,
    }
    const token = await getToken(shop_name).then(data => {
      return data;
    })
    const headers = {
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + token.access_token,
    }
    let results = '';
    if(datax.method == 'edit') {
      results = await axios.put("https://main-api-development.shipdeo.com/v1/couriers/orders/"+ datax.shipdeo_id, body , {
        headers: headers,
      }).then(response => { 
          return response.data;
      })
      .catch(error => {
          return error.response;
      });

    }else{
      results = await axios.post("https://main-api-development.shipdeo.com/v1/couriers/orders", body , {
        headers: headers,
      }).then(response => { 
          return response.data;
      })
      .catch(error => {
          return error.response;
      });
    }
    return results;
    
  }
  

  async function saveOrderToInternal(datax){
    console.log(datax);
    mongoose.connect('mongodb://localhost:27017/shopify',{ useNewUrlParser: true, useUnifiedTopology: true });
    mongoose.set('useFindAndModify', false);
    const db = mongoose.connection;
    db.on('error', console.error.bind(console, 'connection error:'));
    db.once('open', function() {
        var save_order = new Order(datax);
        var query = { order_id: datax.order_id };
        const data = Order.findOneAndUpdate(query ,datax,(error, doc) => {
          if(doc == null) {
            save_order.save(function (err, config) {
              if (err) return console.error(err);
              console.log(config.shop + " saved to collection.");
            });
          }
        });
    });
    //console.log(results);
  }

  async function getOrderShipdeo(param){
    console.log(param);
    const token = await getToken(param.shop).then(data => {
      return data;
    })
    // console.log(token);
    const headers = {
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + token.access_token,
    }
    //console.log(headers);
    const results = await axios.get("https://main-api-development.shipdeo.com/v1/couriers/orders/" + param.shipdeo_id, {
      headers: headers,
    }).then(response => { 
        return response.data;
    })
    .catch(error => {
        return error.response;
    });
    //console.log(results);
    return results;
    
  }

  async function saveOrderLog(shopify_id, shipdeo_id, datax){
    datax.status = 'fail';
    datax.shopify_id = shopify_id;
    datax.shipdeo_id = shipdeo_id;
    if(shipdeo_id){
      datax.status = 'succes';
    }
    mongoose.connect('mongodb://localhost:27017/shopify',{ useNewUrlParser: true, useUnifiedTopology: true });
    mongoose.set('useFindAndModify', false);
    const db = mongoose.connection;
    db.on('error', console.error.bind(console, 'connection error:'));
    db.once('open', function() {
        var save_order = new OrderLog(datax);
        var query = { order_id: datax.order_id };
        const data = OrderLog.findOneAndUpdate(query ,datax,(error, doc) => {
          if(doc == null) {
            save_order.save(function (err, config) {
              if (err) return console.error(err);
              console.log(" saved to collection.");
            });
          }
        });
    });
    //console.log(results);
  }

  router.post("/get_config",async (ctx,next)=>{
    mongoose.connect('mongodb://localhost:27017/shopify',{ useNewUrlParser: true, useUnifiedTopology: true });
    const data_store = ctx.request.body;
    console.log(data_store);
    const data = await Config.findOne( { shop: data_store.shop},{},function (err, res) {
      return res;
    })
    console.log(data)
    ctx.body = data;

  });

  router.post("/save_config",async (ctx,next)=>{
    console.log(ctx.request.body);
    mongoose.connect('mongodb://localhost:27017/shopify',{ useNewUrlParser: true, useUnifiedTopology: true });
    mongoose.set('useFindAndModify', false);
    const db = mongoose.connection;
    db.on('error', console.error.bind(console, 'connection error:'));
    db.once('open', function() {
        const datax = ctx.request.body;
        data_config = { 
          shop: datax.shop, 
          client_id: datax.client_id, 
          client_secret: datax.client_secret, 
        }
        console.log(data_config);
        var save_config = new Config(data_config);
        var query = { shop: datax.shop };
        const data = Config.findOneAndUpdate(query ,data_config,(error, doc) => {
          if(doc == null) {
            save_config.save(function (err, config) {
              if (err) return console.error(err);
              console.log(config.shop + " saved to collection.");
              ctx.body = 'saved to collection.';
            });
          }
        });
    });
    ctx.body = 'test';
  });



  router.get("/process",async (ctx,next)=>{
      mongoose.connect('mongodb://localhost:27017/shopify',{ useNewUrlParser: true, useUnifiedTopology: true });
      const data = await Subdistrict.find({},{},function (err, res) {
        return res;
      })
      await ctx.render('index',{
        name: 'test', 
        subditrict: []
      });

  });

  router.post("/process",async (ctx,next)=>{
      ctx.body = 'getData';
      console.log(ctx.request.body);
  });

  router.post("/cancel_shipping",async (ctx,next)=>{
    const datax = ctx.request.body;
    const token = await getToken(datax.shop).then(data => {
      return data;
    })
    const headers = {
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + token.access_token,
    }
    const results = await axios.delete("https://main-api-development.shipdeo.com/v1/couriers/orders/"+ datax.shipdeo_id, {} , {
      headers: headers,
    }).then(response => { 
        return response.data;
    })
    .catch(error => {
        return error.response;
    });
    ctx.body = 'getData';
    return results;

  });

  router.post("/confirm_order",async (ctx,next)=>{
    const datax = ctx.request.body;
    console.log(datax)
    const token = await getToken(datax.shop).then(data => {
      return data;
    })
    const headers = {
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + token.access_token,
    }
    
    const data_shipdeo = {
        delivery_type: datax.deliver_type,
        destination_lat: datax.data_origin.latitude,
        destination_long: datax.data_origin.longitude,
        destination_subdistrict_code: datax.data_origin.subdistrict_code,
        destination_subdistrict_name: datax.data_origin.subdistrict_name,
        destination_city_code: datax.data_origin.city_code,
        destination_city_name: datax.data_origin.city_name,
        destination_province_code: datax.data_origin.province_code,
        destination_province_name: datax.data_origin.province_name,
        destination_contact_name:  datax.data_origin.contact.name,
        destination_contact_phone:datax.data_origin.contact.phone,
        destination_contact_address: datax.data_origin.contact.address,
        destination_contact_email: datax.data_origin.contact.email,
        destination_note: datax.data_origin.note,
        destination_postal_code: datax.data_origin.postal_code,
    }
    // console.log(data_shipdeo);
    const results = await axios.patch("https://main-api-development.shipdeo.com/v1/couriers/orders/"+ datax.shipdeo_id, data_shipdeo , {
      headers: headers,
    }).then(response => { 
        return response.data;
    })
    .catch(error => {
        return error.response;
    });
    console.log(results);

    const data_locations = await getLocationShopify(datax.shop).then(data => {
      return data;
    })
    // console.log(data_locations);
    const shopify = await ShopifyAccesToken.findOne({ shop: datax.shop },{},function (err, res) {
      return res;
    });

    const headers_shopify = {
      'Content-Type': 'application/json',
      'X-Shopify-Access-Token': shopify.token,
    }
    const body = {
      fulfillment: {
        location_id: data_locations.locations[0].id,
        tracking_number: results.tracking_info.airwaybill,
        tracking_company: results.courier,
        shipment_status: "confirmed",
        status: "open",
        notify_customer: true
      }
    }
    console.log(body);
    const url = 'https://'+datax.shop+'/admin/api/2020-07/orders/'+datax.order_id+'/fulfillments.json';
    const res  = await axios.post(url, body , {headers: headers_shopify});

    const data_internal = {
      order_id : results._id,
      airwaybill : results.tracking_info.airwaybill,
      fulfillment_status: 'In progress'
    }
    console.log(data_internal);
    await saveOrderToInternal(data_internal);

    ctx.body = 'success';

  });


  router.post("/cancel_order",async (ctx,next)=>{
    const datax = ctx.request.body;
    const shopify = await ShopifyAccesToken.findOne({ shop: datax.shop },{},function (err, res) {
      return res;
    });

    const headers = {
      'Content-Type': 'application/json',
      'X-Shopify-Access-Token': shopify.token,
    }
    const url = 'https://'+datax.shop+'/admin/api/2020-07/orders/'+datax.order_id+'/cancel.json';
    const results = await axios.post(url,{}, {headers: headers});
    return results;

  });


  router.post("/save_order",async (ctx,next)=>{
      const datax = ctx.request.body;
      console.log(datax);
      let shop_name = '';
      let url = '';
      if(datax.method == 'edit'){
        url = 'https://'+ datax.shop;
        shop_name = datax.shop;
      }else{
        url = datax.shop;
        shop_name = new URL(datax.shop).host;
      }

      mongoose.connect('mongodb://localhost:27017/shopify',{ useNewUrlParser: true, useUnifiedTopology: true });
      const shopify = await ShopifyAccesToken.findOne({ shop: shop_name },{},function (err, res) {
        return res;
      });
      
      const data_customer = {
        first_name: datax.first_name,
        last_name: datax.last_name,
        email: datax.email,
      }
      const data_billing_address = {
        first_name: datax.first_name,
        last_name: datax.last_name,
        address1:  datax.address,
        phone: datax.phone,
        city: datax.city_name,
        province: datax.province_name,
        country: "Indonesia",
        zip: datax.postal_code,
      }
      const data_shipping_address = {
        first_name: datax.first_name,
        last_name: datax.last_name,
        address1:  datax.address,
        phone: datax.phone,
        city: datax.city_name,
        province: datax.province_name,
        country: "Indonesia",
        zip: datax.postal_code,
      }
      let body = '';
      if(datax.payment === 'cod'){
        body = {
          order: {
            email: datax.email,
            phone: datax.phone,
            send_receipt: true,
            send_fulfillment_receipt: true,
            inventory_behaviour: "decrement_obeying_policy",
            financial_status : "pending",
            line_items: datax.line_items,
            customer: data_customer,
            billing_address: data_billing_address,
            shipping_address: data_shipping_address,
          }
        }
      }else{
        body = {
          order: {
            email: datax.email,
            phone: datax.phone,
            send_receipt: true,
            send_fulfillment_receipt: true,
            inventory_behaviour: "decrement_obeying_policy",
            line_items: datax.line_items,
            customer: data_customer,
            billing_address: data_billing_address,
            shipping_address: data_shipping_address,
          }
        }
      }
      const headers = {
        'Content-Type': 'application/json',
        'X-Shopify-Access-Token': shopify.token,
      }
      let results = '';
      if(datax.method == 'edit'){
        results = await axios.put(url+"/admin/api/2020-07/orders/"+datax.id+".json", body
        , {headers: headers})
        .then(response => { 
          return response.data;
        })
        .catch(error => {
            return error.response;
        });
      }else{
        results = await axios.post(url+"/admin/api/2020-07/orders.json", body
        , {headers: headers})
        .then(response => { 
          return response.data;
        })
        .catch(error => {
            return error.response;
        });
      }
      const res_order = await saveOrderToShipdeo(datax, shop_name);
      if(res_order.data.status == 200){
        let airwaybill = '';
        if(res_order.data.tracking_info.airwaybill){
          airwaybill = res_order.data.tracking_info.airwaybill;
        }
  
        const data_internal = {
          order_id: results.order.id,
          order_id_shipdeo: res_order.data._id,
          shop: shop_name,
          name: results.order.name,
          created_at: results.order.created_at,
          updated_at: results.order.updated_at,
          financial_status: results.order.financial_status,
          fulfillment_status: '',
          item: '',
          total_price: results.order.total_price,
          courier: res_order.data.courier,
          service: res_order.data.service,
          order_number: res_order.data.order_number,
          airwaybill : airwaybill,
          booking_code: res_order.data.tracking_info.booking_code,
        }
  
        await saveOrderToInternal(data_internal);
        await saveOrderLog(res_order.data._id, results.order.id, datax);
        ctx.body = 'success';
      }else{
        ctx.body = res_order.data.message;
      }
      console.log(ctx.body);
     
  });


  router.post("/get_shipping",async (ctx,next)=>{
    mongoose.connect('mongodb://localhost:27017/shopify',{ useNewUrlParser: true, useUnifiedTopology: true });
    const datax = ctx.request.body;
    console.log(datax);
    // let shop_name = '';
    if(datax.method == 'edit'){
       shop_name = datax.shop;
       datax.zip_code = datax.zip_code;
       datax.items = datax.item;
       // console.log(res_orders);
    }else{
       shop_name = new URL(datax.shop).host;
    }
    const data_couriers = await Courier.findOne({ shop: shop_name},{},function (err, res) {
      return res;
    });
    // const store_locations = await StoreLocations.findOne({ shop: shop_name},{},function (err, res) {
    //   return res;
    // })
    const store_locations = await getLocation(shop_name).then(data => {
      return data;
    })
    const body = {
      couriers: data_couriers.couriers,
      origin_lat: '',
      origin_long: '',
      origin_subdistrict_code: store_locations.subdistrict_code,
      origin_subdistrict_name: store_locations.subdistrict_name,
      origin_city_code: store_locations.city_code,
      origin_city_name: store_locations.city_name,
      origin_postal_code: store_locations.zip_code,
      destination_lat: '',
      destination_long: '',
      destination_subdistrict_code: datax.subdistrict_code,
      destination_subdistrict_name: datax.subdistrict_name,
      destination_city_code: datax.city_code,
      destination_city_name: datax.city_name,
      destination_postal_code: datax.zip_code,
      is_cod: true,
      items: datax.items
    }
    console.log(JSON.stringify(body));
    try {
      const token = await getToken(shop_name).then(data => {
        return data;
      })
      const headers = {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + token.access_token,
      }
      const results = await axios.post("https://main-api-development.shipdeo.com/v1/couriers/pricing", body, {
        headers: headers,
      })
      .then(res => {
        ctx.body = res.data;
      });
    } catch (err) {
      console.log(err)
    }
    // ctx.body = 'tes';
  });

  router.post('/api/list_courier', async (ctx) => {
    const datax = ctx.request.body;
    try {
      const token = await getToken(datax.shop).then(data => {
        return data;
      })
      const headers = {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + token.access_token,
      }
      console.log(headers);
      const results = await axios.get("https://main-api-development.shipdeo.com/v1/couriers", {
        headers: headers,
      })
      .then(res => {
        console.log(res);
        ctx.body = res.data.data;
      });
      // console.log(results);
    } catch (err) {
      console.log(err)
    }
  })

  router.post("/manage_courier",async (ctx,next)=>{
    mongoose.connect('mongodb://localhost:27017/shopify',{ useNewUrlParser: true, useUnifiedTopology: true });
    const db = mongoose.connection;
    db.on('error', console.error.bind(console, 'connection error:'));
    db.once('open', function() {
        console.log("Connection Successful!");
        const datax = ctx.request.body;
        const data_courier = {
            shop: datax.shop, 
            couriers: datax.courier 
        }
        var save_courier = new Courier(data_courier);
        var query = { shop: datax.shop };
        const data = Courier.findOneAndUpdate(query ,data_courier,(error, doc) => {
          if(doc == null) {
            save_courier.save(function (err, courier) {
              if (err) return console.error(err);
              console.log(courier.shop + " saved to courier collection.");
              ctx.body = 'saved to courier collection.';
            });
          }
        });
    });
    ctx.body = 'test';
  });


  router.post("/manage_feature",async (ctx,next)=>{
    mongoose.connect('mongodb://localhost:27017/shopify',{ useNewUrlParser: true, useUnifiedTopology: true });
    const db = mongoose.connection;
    db.on('error', console.error.bind(console, 'connection error:'));
    db.once('open', function() {
        console.log("Connection Successful!");
        const datax = ctx.request.body;
        const data_features = {
            shop: datax.shop, 
            features: datax.courier
        }
        var save_feature = new Features(data_features);
        var query = { shop: datax.shop };
        console.log(query);
        const data = Features.findOneAndUpdate(query ,data_features,(error, doc) => {
          console.log(doc);
          if(doc == null) {
            save_feature.save(function (err, feature) {
              if (err) return console.error(err);
              console.log(feature.shop + " saved to feature collection.");
              ctx.body = 'saved to feature collection.';
            });
          }
        });
        // save model to database
    });
    ctx.body = 'test';
  });

  router.post("/get_features",async (ctx,next)=>{
    const data_courier = ctx.request.body;
    mongoose.connect('mongodb://localhost:27017/shopify',{ useNewUrlParser: true, useUnifiedTopology: true });
    const data = await Features.findOne( { shop: data_courier.shop},{},function (err, res) {
      return res;
    })
    ctx.body = data;
  
  });

  
  router.post("/save_location",async (ctx,next)=>{
    mongoose.connect('mongodb://localhost:27017/shopify',{ useNewUrlParser: true, useUnifiedTopology: true });
    mongoose.set('useFindAndModify', false);
    const db = mongoose.connection;
    db.on('error', console.error.bind(console, 'connection error:'));
    db.once('open', function() {
        const datax = ctx.request.body;
        // console.log(datax);
        data_location = { 
          shop: datax.shop, 
          phone_number: datax.phone_number, 
          address: datax.address, 
          zip_code: datax.zip_code, 
          subdistrict_code: datax.subdistrict_code, 
          subdistrict_name: datax.subdistrict_name, 
          city_code: datax.city_code, 
          city_name: datax.city_name, 
          province_code: datax.province_code, 
          province_name: datax.province_name, 
          email: datax.email,
          note: datax.note,
        }
        console.log(data_location);
        var save_location = new StoreLocations(data_location);
        var query = { shop: datax.shop };
        const data = StoreLocations.findOneAndUpdate(query ,data_location,(error, doc) => {
          if(doc == null) {
            save_location.save(function (err, location) {
              if (err) return console.error(err);
              console.log(location.shop + " saved to collection.");
              ctx.body = 'saved to collection.';
            });
          }
        });
    });
    ctx.body = 'test';
  });

  router.post("/get_order",async (ctx,next)=>{
    mongoose.connect('mongodb://localhost:27017/shopify',{ useNewUrlParser: true, useUnifiedTopology: true });
    const datax = ctx.request.body;
    console.log(datax);
    const data = await Order.find( { shop: datax.shop},{},function (err, res) {
      return res;
    })
    ctx.body = data;

  });


  router.post("/detail_order",async (ctx,next)=>{
    const  param = ctx.request.body;
    const dataShopify = await getShopify(param);
    //console.log(dataShopify);
    const dataShipdeo = await getOrderShipdeo(param);
    //console.log(dataShipdeo);
    const result = {
      shopify: dataShopify.data.order,
      shipdeo: dataShipdeo.data
    }
   //  console.log(JSON.stringify(result));
    ctx.body = result;
  
  });

  router.post("/edit_order",async (ctx,next)=>{
    const  param = ctx.request.body;
   // console.log(param);

    const orders = await getOrderShipdeo(param);
    // console.log(orders);
    ctx.body = orders.data;
   // console.log(get_data);
    

  });


  router.post("/get_account",async (ctx,next)=>{
    mongoose.connect('mongodb://localhost:27017/shopify',{ useNewUrlParser: true, useUnifiedTopology: true });
    const datax = ctx.request.body;
    const shop_name = new URL(datax.shop).host;
    const data = await StoreLocations.findOne( { shop: shop_name},{},function (err, res) {
      return res;
    })
    console.log(data);
    ctx.body = data;

  });

  router.post("/get_waybill",async (ctx,next)=>{
    const datax = ctx.request.body;
    console.log(datax)
    const body = {
      courier_code: datax.courier,
      waybill: datax.airwaybill,
    }
    const token = await getToken(datax.shop).then(data => {
      return data;
    })
    const headers = {
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + token.access_token,
    }
    console.log(body)
    console.log(headers)
    const results = await axios.post("https://main-api-development.shipdeo.com/v1/couriers/waybill", body, {
      headers: headers,
    }).then(response => { 
        return response.data;
    })
    .catch(error => {
        return error.response;
    });    
    console.log(results);
    ctx.body = results;

  });



  router.post("/get_location",async (ctx,next)=>{
    mongoose.connect('mongodb://localhost:27017/shopify',{ useNewUrlParser: true, useUnifiedTopology: true });
    const data_store = ctx.request.body;
    console.log(data_store);
    const data = await StoreLocations.findOne( { shop: data_store.shop},{},function (err, res) {
      return res;
    })
    ctx.body = data;

  });
  


  router.get("/master_locations", async (ctx,next)=>{
    mongoose.connect('mongodb://localhost:27017/shopify',{ useNewUrlParser: true, useUnifiedTopology: true });
    const data = await Subdistrict.find({},{},function (err, res) {
      return res;
    })
    ctx.body = data;

  });

  router.post("/get_courier",async (ctx,next)=>{
    const data_courier = ctx.request.body;
    mongoose.connect('mongodb://localhost:27017/shopify',{ useNewUrlParser: true, useUnifiedTopology: true });
    const data = await Courier.findOne( { shop: data_courier.shop},{},function (err, res) {
      return res;
    })
    ctx.body = data;
  
  });
  
  //router.get('/(.*)', async (ctx, next) => {
  //router.get('/(.*)', async (ctx) => {
  router.get('/(.*)', verifyRequest(), async (ctx) => {
    await handle(ctx.req, ctx.res);
    ctx.respond = false;
    ctx.res.statusCode = 200;
  });

  //router.post('/(.*)', verifyRequest(), async (ctx) => {
  router.post('/(.*)', async (ctx) => {
    await handle(ctx.req, ctx.res);
    ctx.respond = false;
    ctx.res.statusCode = 200;
  });

  server.use(router.allowedMethods());
  server.use(router.routes());

  server.listen(port, () => {
    console.log(`> Ready on http://localhost:${port}`);
  });
});